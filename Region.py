
class Region(object):
    """
    be used to set rocktype
    """
    xmin = 0
    xmax = 0
    ymin = 0
    ymax = 0
    zmin = 0
    zmax = 0

    def __init__(self, xmin = 0, xmax = 0, ymin = 0, ymax = 0, zmin = 0, zmax = 0):
        if xmax < xmin or ymax < ymin or zmax < zmin:
            import os
            sys.exit("region value setting error")
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.zmin = zmin
        self.zmax = zmax