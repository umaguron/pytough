"""
MINCなしで定常状態計算した SAVEを元に
MINCも入れた計算用のINCONを作成する
MINCを考慮した定常状態の計算が時間かかりすぎるので作成。
"""

from t2data import *
from t2incons import *
from t2listing import *
import os
import datetime
import math
from Region import *

xarr0 = np.logspace(math.log10(600), math.log10(4000), 5)
xarr1 = np.logspace(math.log10(400), math.log10(1000), 4)
xarrCore = [400]*5
xarr2 = np.flip(xarr1)
xarr3 = np.flip(xarr0)
xarr = np.r_[xarr2, xarrCore, xarr1]
dz1 = np.logspace(math.log10(20), math.log10(400), 5)
dz2 = [400]*10
zarr = np.r_[dz1, dz2]

usesSaveAsIncon = True 
geo_y_logscale_400_2_dykes = mulgrid().rectangular(\
    np.r_[xarr3, xarr, xarr0], \
    np.r_[\
        np.logspace(math.log10(4000), math.log10(400), 6), \
        [400]*2, \
        np.logspace(math.log10(230), math.log10(20), 4), \
        np.logspace(math.log10(45), math.log10(230), 3), \
        [341]*3, \
        np.logspace(math.log10(230), math.log10(20), 4), \
        np.logspace(math.log10(45), math.log10(230), 3), \
        np.logspace(math.log10(400), math.log10(4000), 6)\
    ], \
    zarr, \
    atmos_type = 0)

geo = geo_y_logscale_400_2_dykes
geo.atmosphere_volume = 1e50
geo.atmosphere_connection = 1e-9

# Create TOUGH2 input data file:
dat = t2data()
dat.grid = t2grid().fromgeo(geo)



resv = Region( \
    xmin = np.sum(np.r_[xarr1, xarr0]), \
    xmax = np.sum(np.r_[xarr1, xarr0]) + 2000, \
    ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(101.734), 2)) - 3000, \
    ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(101.734), 2)), \
    zmin = -3000, \
    zmax = -1500 \
    )
dyke_ms = Region( \
    xmin = np.sum(np.r_[xarr1, xarr0]) + 800, \
    xmax = np.sum(np.r_[xarr1, xarr0]) + 1200, \
    ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) - 20, \
    ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)), \
    zmin = -1500, \
    zmax = -740 \
    )
dyke_sn = Region( \
    xmin = np.sum(np.r_[xarr1, xarr0]) + 800, \
    xmax = np.sum(np.r_[xarr1, xarr0]) + 1200, \
    ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) - 20, \
    ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)),\
    zmin = -10000, \
    zmax = -341 \
    )

# MINC
# [vol fraction of FRACTURE, vol fraction of MINC level 1, ...]
volume_fraction = [0.01, 0.99]
# fractureの間隔(m) num_fracture_planes>=2のときはlist
fracture_spacing = 1
# 1, 2 or 3
number_fracture_planes = 1

if True:
    minc_block_list = []
    for blk in dat.grid.blocklist:
        if 'ATM' not in blk.name :
            # reservoir
            if resv.xmin <= blk.centre[0] <= resv.xmax \
                    and resv.ymin <= blk.centre[1] <= resv.ymax \
                    and resv.zmin <= blk.centre[2] <= resv.zmax :
                minc_block_list.append(blk.name)
                continue
            # dyke
            if dyke_ms.xmin <= blk.centre[0] <= dyke_ms.xmax \
                    and dyke_ms.ymin <= blk.centre[1] <= dyke_ms.ymax \
                    and dyke_ms.zmin <= blk.centre[2] <= dyke_ms.zmax :
                minc_block_list.append(blk.name)
                continue
            if dyke_sn.xmin <= blk.centre[0] <= dyke_sn.xmax \
                    and dyke_sn.ymin <= blk.centre[1] <= dyke_sn.ymax \
                    and dyke_sn.zmin <= blk.centre[2] <= dyke_sn.zmax :
                minc_block_list.append(blk.name)

    # 既存のグリッドをMINC対応版に更新する
    minc_indices = dat.grid.minc(\
        # [vol fraction of FRACTURE, vol fraction of MINC level 1, ...]
        volume_fraction, \
        # fractureの間隔(m) num_fracture_planes>=2のときはlist
        spacing=fracture_spacing, \
        # 1, 2 or 3
        num_fracture_planes=number_fracture_planes, \
        # block name list
        blocks=minc_block_list, \
        # MINC　matrixのblocknameを返す関数。Noneの場合もともとの名前のはじめ１文字をMINC levelの数字に書き換え
        matrix_blockname=None, \
        # MINC　matrixのrock nameを返す関数. Noneの場合もともとのROCK名のはじめ１文字を'X'に書き換え
        minc_rockname=None, \
        # fracture面から与えられた長さ以内にあるmatrixの総体積。 Noneの場合num_fracture_planesに応じて自動で計算
        proximity=None, \
        # 空気層の体積
        atmos_volume=1e50, \
        # t2inconを与えると適切に書き換えてくれる
        incon=None\
    )

# INCON
# 元になるSAVE
inconDir = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190904-215436_no_minc_mesh_logscale_400_2_dykes_hydrostatic/"
dat.incon = None
if usesSaveAsIncon and os.path.exists(inconDir):
    # SAVEファイルをINCONとして使用する場合は今回のフォルダにINCONの形式で書き出す。
    incon = t2incon(inconDir + 'SAVE')
    # MINCブロック(fracture)のインデックスを取得
    for minc_before_block_index in minc_indices[0]:
        # ブロック名を表示
        nm = dat.grid.blocklist[minc_before_block_index].name
        print(nm)
        # ３つのprimary variables
        print(incon.variable[minc_before_block_index])
        # assign MINC matrix block's PV
        minc_matrix_name = '1' + nm[1:5]
        incon[minc_matrix_name][0] = incon.variable[minc_before_block_index][0]
        incon[minc_matrix_name][1] = incon.variable[minc_before_block_index][1]
        incon[minc_matrix_name][2] = incon.variable[minc_before_block_index][2]
        incon[minc_matrix_name].porosity = 0.05
    incon.write(inconDir + 'MINCmanualIncon')
    
    print("\nUSE SAVE AS INCON\n")
else:
    dat.incon = t2incon()
    print("\nUSE DEFAULT INCON\n")