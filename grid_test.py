from t2data import *
from t2incons import *

#------3D----------------------------------------------------
# Create geometry- grid sizes are constant 1000 x 800 m horizontal,
# and 15 vertical layers increasing logarithmically in thickness from
# 10 m at surface to 100 m at bottom:
dx3d = [1000.]*10
dy3d = [800.]*12
dz3d = np.logspace(1., 2., 15)
geo3d = mulgrid().rectangular(dx3d, dy3d, dz3d, atmos_type = 0)

# Create TOUGH2 input data file:
dat3d = t2data()
dat3d.title = 'Hydrostatic 3D example'
dat3d.grid = t2grid().fromgeo(geo3d)
dat3d.parameter.update(
    {'max_timesteps': 999,
     'tstop': 1.e14,
     'const_timestep': 1.e7,
     'print_interval': 20,
     'gravity': 9.81,
     'default_incons': [101.3e3, 20.]})
dat3d.start = True

# Set MOPs:
dat3d.parameter['option'][1] = 1
dat3d.parameter['option'][16] = 5

# Set relative permeability (Corey) and capillarity functions:
dat3d.relative_permeability = {'type': 3, 'parameters': [0.3, 0.1, 0., 0., 0.]}
dat3d.capillarity = {'type': 1, 'parameters': [0., 0., 1., 0., 0.]}

# Add a second rocktype, with anisotropic permeability:
r2 = rocktype('rock2', permeability = [0.6e-15]*2 + [0.3e-15])
dat3d.grid.add_rocktype(r2)
# Assign it to blocks below -100 m elevation:
for blk in dat3d.grid.blocklist[1:]:
    if blk.centre[2] <= -100: blk.rocktype = r2

# (Note: we skipped the first block (dat3d.grid.blocklist[0]) which is
# the atmosphere and has no centre defined)

#----1D----------------------------------------------------------
length = 200.
nblks = 50
dx1d = [length / nblks] * nblks
dy1d = dz1d = [1.0]
geo1d = mulgrid().rectangular(dx1d, dy1d, dz1d)

# Create TOUGH2 input data file:
dat1d = t2data()
dat1d.title = 'Horizontal 1D example'
dat1d.grid = t2grid().fromgeo(geo1d)
dat1d.parameter.update(
    {'max_timesteps': 200,
     'tstop': 1.e10,
     'const_timestep': 100.,
     'print_interval': 20,
     'gravity': 9.81,
     'default_incons': [101.3e3, 20.]})
dat1d.start = True

# Set MOPs:
dat1d.parameter['option'][1] = 1
dat1d.parameter['option'][16] = 5

# add boundary condition block at each end:
bvol = 0.0
conarea = dy1d[0] * dz1d[0]
condist = 1.e-6

b1 = t2block('bdy01', bvol, dat1d.grid.rocktype['dfalt'])
dat1d.grid.add_block(b1)
con1 = t2connection([b1, dat1d.grid.blocklist[0]],
                    distance = [condist, 0.5*dx1d[0]], area = conarea)
dat1d.grid.add_connection(con1)

b2 = t2block('bdy02', bvol, dat1d.grid.rocktype['dfalt'])
dat1d.grid.add_block(b2)
con2 = t2connection([dat1d.grid.blocklist[nblks-1], b2],
                    distance = [0.5*dx1d[nblks-1], condist], area = conarea)
dat1d.grid.add_connection(con2)

# Set initial condition at x = 0:
dat1d.incon['bdy01'] = [None, [120.e3, 200.]]

##---------- radial-------------------------------------------------
from math import log10
# mesh parameters:
well_radius = 0.1
num_blocks = 30
dr_min, dr_max = 0.1, 1000.
layer_thickness = 10.

drRad = np.logspace(log10(dr_min), log10(dr_max), num_blocks)
dzRad = np.array([layer_thickness])
orig = np.array([well_radius, 0., 0.])

# create TOUGH2 input data:
datRadial = t2data()
datRadial.title = 'Radial model' 
datRadial.grid = t2grid().radial(drRad, dzRad, origin = orig)
datRadial.grid.rocktype['dfalt'].permeability[:2] = 50.e-15
datRadial.parameter.update(
    {'const_timestep': 0.1,
     'max_timesteps': 1000,
     'tstop': 6. * 3600.,
     'print_interval': 20,
     'gravity': 9.81,
     'default_incons': [3.e5, 20.]})
datRadial.parameter['option'][1] = 1
datRadial.parameter['option'][16] = 5
datRadial.start = True

# add generator:
flow_rate = -0.1
gen = t2generator(name = 'wel 1', block = datRadial.grid.blocklist[0].name,
                  gx = flow_rate, type = 'MASS')
datRadial.add_generator(gen)

#-----print--------------------
from util import *
print("-----------------------geo1d-----------------------")
#util.printMulPrp(geo1d)

print()
print("-----------------------geo3d-----------------------")
#util.printMulPrp(geo3d)

dx=[10]*3
dy=[10]*3
dz=[5]*2
geo = mulgrid().rectangular(dx, dy, dz, atmos_type = 1)
lay1=layer('lay1', 0.0, 0.25, 0.5)
lay2=layer('lay2', -10, -12.5, -15)
geo.add_layer(lay2)
geo.add_layer(lay1)
print(lay2)
print(lay2.get_thickness())
print()
#util.printMulPrp(geo)
print(geo.nodelist[0].name)
print(geo.nodelist[0].pos)
print(geo.nodelist[0].column)
print(geo.nodelist[1].name)
print(geo.nodelist[1].pos)
print(geo.nodelist[1].column)
print()
print(geo.columnlist[1].name)
print(geo.columnlist[1].centre)
print(geo.columnlist[1].surface)
print(geo.columnlist[1].node)
print()
"""
print(geo.welllist[0].bottom)
print(geo.welllist[0].deviated)
print(geo.welllist[0].head)
print(geo.welllist[0].name)
print(geo.welllist[0].num_pos)
print(geo.welllist[0].num_deviations)
print(geo.welllist[0].pos)
"""
print(geo)
util.printMulPrp(geo)

print("******** dat1d********")
util.gridPrp(dat1d.grid)
print("******** dat3d********")
util.gridPrp(dat3d.grid)
print("******** datRadial*********")
util.gridPrp(datRadial.grid)
