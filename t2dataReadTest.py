"""
EOS3 sam1の読み込み。
要素の情報が別ファイルMESHに書かれている.
FOFT,GOFTがあることでread()でエラーになってしまったので、sam1より分離して読み込ませる

sam1+MESH-FOFT-GOFT → sam1_mesh_included
"""
from t2data import *
from mulgrids import *

dat = t2data()
dat.read('/Users/matsunagakousei/Documents/資料/TOUGH2/Sample_EOS3/sam1_mesh_included')

dat.parameter
# FOFTは別で読み込む
infile = t2data_parser('/Users/matsunagakousei/Documents/資料/TOUGH2/Sample_EOS3/sam1_FOFT', 'rU')
dat.read_history_blocks(infile)

# GOFT

print(dat.grid.rocktype)
print(dat.parameter)
print(dat.multi)
print(dat.start)
print(dat.solver)
print(dat.relative_permeability)
#print(dat.grid.blocklist)
#print(dat.grid.connectionlist)
print(dat.generator)
print(dat.incon)
print(dat.indom)
print(dat.history_block)
print(dat.generator)
print(dat.generatorlist[0].ex)

geo = dat.grid.rectgeo()