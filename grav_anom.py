
"""
重力異常の観測点の座標が(0,0,0)
指定範囲内にある領域のうちfracrure_volume_fractの割合で欠損し密度delta_rho分だけ減少したときの重力異常の値を計算
"""

import math 

G = 6.67430e-11 #m3 kg−1 s−2
delta_rho = 2.6e3 - 1e3 #kg/m3
fracrure_volume_fract = 0.05
zmin = 1540
zmax = 3140
dz = 10
ymin = -1500
ymax = 1500
dy = dz
xmin = -1000
xmax = 1000
dx = dz

delta_g = 0
for z in range(zmin, zmax, dz):
    for y in range(0, ymax, dy):
        for x in range(0, xmax, dx):
            # print(str(x) + ', ' + str(y) + ', ' + str(z))
            dv = dz * dy * dx
            r = math.sqrt((x-0.5*dx)**2 + (y-0.5*dy)**2 + (z-0.5*dz)**2)
            # R = math.sqrt(x^2 + y^2)
            cos = z / r
            delta_g += (G * dv * delta_rho * fracrure_volume_fract  * cos) / r**2

print(str(delta_g * 4 * 1e5) + 'mgal')