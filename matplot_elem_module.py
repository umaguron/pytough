from t2listing import *
from t2data import *
import numpy as np
import os
from matplotlib.backends.backend_pdf import PdfPages
import re
import matplotlib.pyplot as plt
import matplotlib as mpl


listing_fp = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190918-215134_ws_200_0p05_10_-12_stc1_restrt/20190918-215134.listing"
listing_fp = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190918-213604_ws_200_0p1_10_-12_test/20190918-213604.listing"
listing_fp = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190918-171040_ws_200_0p1_10_-11_hydrstc/20190918-171040.listing"
# listing_fp = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190918-153231_ws_200_0p1_10_-12_test/20190918-153231.listing"

elems = [' je 6', ' sa 5', ' sa 8', ' sa11', ' sa15']
params = ['P', 'T', 'PCO2']
# elems = [' je 6']
# params = ['P', 'T']


def plot_param_time(listing_fp, elem_nm_list, param_list):
    """
    listing_fp: 読み込むlistingファイルのパス
    elem_nm_list: 結果をプロットしたい要素名
    渡されたlistingファイルを読み、各パラメータの時系列変化をプロットする。
    結果はpdfでlistingファイルのある階層に保存する
    """
    # はじめに結果ファイルの存在確認
    if os.path.exists(listing_fp):
        save_fp = re.sub("[^/]*listing", "result_plot.pdf",  listing_fp)
        num_elem = len(elem_nm_list)
        # 結果の読み込み
        lst = t2listing(listing_fp)
        
        # check convergence
        # print(lst.convergence['T'])
        # print(lst.convergence['P'])
        # print(lst.convergence['SG'])
        # print(lst.convergence['PCO2'])
        # print(lst.convergence['XCO2(av)'])

        # matplot setting
        plt.rcParams['font.family'] ='sans-serif'#使用するフォント
        plt.rcParams['xtick.direction'] = 'in'#x軸の目盛線が内向き('in')か外向き('out')か双方向か('inout')
        plt.rcParams['ytick.direction'] = 'in'#y軸の目盛線が内向き('in')か外向き('out')か双方向か('inout')
        plt.rcParams['xtick.major.width'] = 1.0#x軸主目盛り線の線幅
        plt.rcParams['ytick.major.width'] = 1.0#y軸主目盛り線の線幅
        plt.rcParams['font.size'] = 8 #フォントの大きさ
        plt.rcParams['axes.linewidth'] = 1.0# 軸の線幅edge linewidth。囲みの太さ
        plt.rcParams['axes.labelsize'] = 'medium'
        plt.rcParams['axes.grid'] = True
        mksize = 3 # マーカーの大きさ
        size_x = 19 # cm
        size_y = 7 * num_elem # cm
        
        # プロットの配置　行・列の数
        num_row = num_elem
        num_col = 3
        # 現在プロットしている場所の番号
        plt_pos = 1

        # figure作成
        fig = plt.figure(figsize=(size_x * 3.14 / 8, size_y * 3.14 / 8))
        plt.clf()


        # タイムステップ数数カウント用
        lc=0

        # 時系列取得
        # 読み込み位置リセット
        lst.first()
        # 取得パラメーター初期化
        t = []
        res_arr = []
        for a in elem_nm_list:
            tmp = []
            for b in param_list:
                tmp.append([])
            res_arr.append(tmp)
        # 取得
        while lst.next():
            lc+=1  
            # 時間軸取得
            t.append(lst.time)
            # 要素ごと
            for en, elem_nm in enumerate(elem_nm_list):
                # パラメーターの種類毎に結果を格納
                for pn, param in enumerate(param_list):
                    res_arr[en][pn].append(lst.element[elem_nm][param])
        print("timeseries retrieved;  num timestep: " + str(lc))
                
        # 取得した時系列のプロット
        for en, elem_nm in enumerate(elem_nm_list):
            print("elem name: " + elem_nm)
            for pn, param in enumerate(param_list):
                ax = plt.subplot(num_row, num_col, plt_pos) # subplot returns axis 
                ax.set_title(param + ' @ ' + elem_nm)
                plt.plot(t, res_arr[en][pn], marker='o', markersize=mksize, color='b', label=param + ' at ' + elem_nm )
                plt.xlabel('time(second)')
                plt.ylabel(param)
                plt_pos += 1
                print("  " + param + " plotted")

        plt.tight_layout()

        # set path
        pp = PdfPages(save_fp)

        # save figure
        pp.savefig(fig, transparent=True)
        print("saved at " + save_fp)

        # close file
        pp.close()

        # geo.layer_plot(-600, np.insert(lst.element['T'], 0, 0), 'Temperature', '$\degree$C', contours = np.arange(100,200,25), flow=lst.connection['FLO(AQ.)'])
        # geo.slice_plot([np.array([0, 550]), np.array([1100, 550])], np.insert(lst.element['T'], 0, 0), 'Temperature', '$\degree$C', flow=lst.connection['FLO(AQ.)'], )
        #geo.line_plot([500, 500, 0], [500, 500, 881.1], lst3.)

    else:
        print("no listing file")

# plot_param_time(listing_fp, elems, params)
