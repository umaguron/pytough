"""
EOS2 injection test
7/14
atmos_type = 0
 'default_incons': [100, 0.01, 101.3e1]　→収束する　hydrostaticに達する
atmos_type = 2
 'default_incons': [100, 0.01, 101.3e1]　→収束しない hydrostaticに達しない


"""

from t2data import *
from t2incons import *

geofilename = 'geom.dat'
#dat_file_base_name = 'inj_test_hydrostatic101012_100_0-01_101-3e1' # ピリオド.は使用不可
dat_file_base_name = 'inj_test_hydrostc'
dx = 100.
nx = 10
dy = 100.
ny = 10

# make mulgird
xarr = [dx]*nx
yarr = [dy]*ny
dz1 = np.logspace(1., 2.30102999566, 5)
dz2 = [100]*8
geo = mulgrid().rectangular(xarr, yarr, np.r_[dz1, dz2], atmos_type = 0)
geo.write(geofilename)


# Create TOUGH2 input data file:
dat = t2data()
dat.title = 'inj test 3d 1'
dat.grid = t2grid().fromgeo(geo)

# PARAM
dat.parameter.update(
    {'max_timesteps': 999,
     'print_level': 1,
     'tstop': 1.e15,
     'const_timestep': 1.e7,
     'print_interval': 3,
     'gravity': 9.81,
     'default_incons': [100, 0.01, 101.3e1]})

# Set MOPs:
dat.parameter['option'][1] = 1 # *** ALLOWS TO GENERATE A SHORT PRINTOUT FOR EACH NEWTON-RAPHSON ITERATION

#   MORE PRINTOUT IS GENERATED FOR MOP(I) > 0 IN THE FOLLOWING SUBROUTINES (THE LARGER MOP IS, THE MORE WILL BE PRINTED)
dat.parameter['option'][2] = 0 # *** CYCIT
dat.parameter['option'][3] = 0 # *** MULTI 
dat.parameter['option'][4] = 0 # *** QU
dat.parameter['option'][5] = 0 # *** EOS
dat.parameter['option'][6] = 0 # *** LINEQ
dat.parameter['option'][8] = 0 # *** DISF (T2DM ONLY)

dat.parameter['option'][7] = 0 # *** IF UNEQUAL ZERO, WILL GENERATE A PRINTOUT OF INPUT DATA
dat.parameter['option'][9] = 0 # *** CHOOSES FLUID COMPOSITION ON WITHDRAWAL (PRODUCTION). = 0: ACCORDING TO RELATIVE MOBILITIES.= 1: ACCORDING TO COMPOSITION IN PRODUCING ELEMENT.
dat.parameter['option'][16] = 5 # *** PERMITS TO CHOOSE TIME STEP SELECTION OPTION  = 0: USE TIME STEPS EXPLICITLY PROVIDED AS INPUT. > 0: INCREASE TIME STEP BY AT LEAST A FACTOR 2, IF CONVERGENCE OCCURS IN .LE. MOP(16) ITERATIONS.
dat.parameter['option'][17] = 9 #  *** HANDLES TIME STEPPING AFTER LINEAR EQUATION SOLVER FAILURE. = 0: NO TIME STEP REDUCTION DESPITE LINEAR EQUATION SOLUTION FAILURE. = 9: REDUCE TIME STEP AFTER LINEAR EQUATION SOLUTION FAILURE.

# set short printout
# PARAM - ELST 
dat.parameter['print_block'] = '  w 4'
# FOFT
dat.history_block = ['  w 4']

dat.start = True

# MULTI 
dat.multi = {
    'num_components': 2,
    'num_equations': 3,
    'num_phases': 2,
    'num_secondary_parameters': 6} 


# ROCKS
# define rock
resevoir = rocktype(name = "resvr", nad = 0, density = 2650.0, porosity = 0.1, permeability = [1e-14]*3, conductivity = 2.1, specific_heat = 1000.0)
around = rocktype(name = "arnd", nad = 0, density = 2650.0, porosity = 0.1, permeability = [1e-15]*3, conductivity = 2.1, specific_heat = 1000.0)

# 
dat.grid.write_vtk(geo, dat_file_base_name + '_grid.vtu')

# hydrostatic state を計算
dat.write(dat_file_base_name + '.dat')

# --- run the model ------------------------------------
import os
file = os.path.abspath(dat_file_base_name + '.dat')
os.system('t2exec 2 ' + file)


from t2listing import *
lst = t2listing(dat_file_base_name + '.listing')
lst.write_vtk(geo, dat_file_base_name + '.vtu', None)