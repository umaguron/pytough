"""
rvf
"""
from mulgrids import *
from t2data import *
from mulgrids import *
from util import *

# calcurate hydrostatic pressure equilibrium
dat = t2data()
dat.read('/Users/matsunagakousei/Documents/資料/TOUGH2/Sample_EOS1/rvf2')

#gridを作成
dx = [20] * 12
dz = [20] * 10
dy = [0.04]
geo = mulgrid().rectangular(dx, dy, dz)
geo.write('rvf_geom.dat')

# gridを既存のものと置き換え
dat.grid = t2grid().fromgeo(geo)
# define rock
fract = rocktype(name = "FRACT", nad = 0, density = 2650.0, porosity = 0.5, permeability = [200e-12]*3, conductivity = 0.0, specific_heat = 1000.0)
conbd = rocktype(name = "CONBD", nad = 0, density = 2650.0, porosity = 0.0, permeability = [0.0]*3, conductivity = 2.1, specific_heat = 1000.0)

# ROCKSに追加
dat.grid.add_rocktype(fract)
dat.grid.add_rocktype(conbd)

for blk in dat.grid.blocklist:
    blk.rocktype = fract

dat.grid.write_vtk(geo, 'rvf2grid.vtu')

#util.t2dataPrp(dat, '  a 1')
#util.gridPrp(dat.grid)

dat.write('rvf2.dat')




# --- run the model ------------------------------------
import os
file = os.path.abspath('rvf2.dat')
#os.system('t2exec 1 ' + file)


from t2listing import *
lst = t2listing('rvf2.listing')
lst.write_vtk(geo, 'rvf2.vtu', None)



# rvf3
dat3 = t2data()
dat3.read('/Users/matsunagakousei/Documents/資料/TOUGH2/Sample_EOS1/rvf3')


# gridを既存のものと置き換え
dat3.grid = t2grid().fromgeo(geo)
# define rock
fract = rocktype(name = "FRACT", nad = 0, density = 2650.0, porosity = 0.5, permeability = [200e-12]*3, conductivity = 0.0, specific_heat = 1000.0)
conbd = rocktype(name = "CONBD", nad = 0, density = 2650.0, porosity = 0.0, permeability = [0.0]*3, conductivity = 2.1, specific_heat = 1000.0)

# ROCKSに追加
dat3.grid.add_rocktype(fract)
dat3.grid.add_rocktype(conbd)

for blk in dat3.grid.blocklist:
    blk.rocktype = fract

#### add 20190707
# set interface area for heat exchange (AHTXの設定) 
# to calc semi analytical heat excange, set MOP15 >0
for blk in dat3.grid.blocklist:
    blk.ahtx = 0.8e+3

#### add 20190708 set ELST and FOFT
#print(dat3.parameter)
dat3.parameter['print_block'] = '  l 3'
#print(dat3.history_block)
dat3.history_block = ['  l 3']


"""
INCONはrvf2のシミュレーション結果を用いるので以下不要
# INCON追加 →　hydrostatic
inc = dat3.grid.incons()
rho = 998.
g = 9.8
# PARAM - DEP(1)
P0 = dat.parameter['default_incons'][0]
# PARAM - DEP(2)
T0 = dat.parameter['default_incons'][1]
print(P0)
for blk in dat.grid.blocklist:
    h = -blk.centre[2] 
    P = P0 + rho * g * h
    T = T0
    inc[blk.name].variable = [P, T]
inc.write('INCON')
"""

# GENERの設定
gen_inj = t2generator(name = 'INJ 1', block = '  a 8', type = 'COM1', gx = 4.0, ex = 4.2e+5 )
# when type = DELV, gx is productivity index PI(m^3), ex is pressure (Pa)  9.65e+6 ~ hydrostatic より100Paほど小さい値
gen_pro = t2generator(name = 'PRO 1', block = '  l 3', type = 'DELV', gx = 4.0e-12, ex = 9.65e+6)

"""
以下のように設定するのは間違い？
# EL, NE
dat3.generator['INJ 1'].block = '  a 8'
dat3.generator['PRO 1'].block = '  l 3'

# TYPE
dat3.generator['INJ 1'].type = 'COM1'
dat3.generator['PRO 1'].type = 'DELV'

# GX
dat3.generator['INJ 1'].gx = 4.
dat3.generator['PRO 1'].gx = 4.0e-12

# EX
dat3.generator['INJ 1'].ex = 4.2e+5
dat3.generator['PRO 1'].ex = 9.65e+6
"""

print(dat3.generator)
print(dat3.generator[('A1801', 'INJ 1')].block)
print(dat3.generator[('A1801', 'INJ 1')].type)
print(dat3.generator[('A1801', 'INJ 1')].gx)
print(dat3.generator[('A1801', 'INJ 1')].ex)
print(dat3.generator[('A1312', 'PRO 1')].block)
print(dat3.generator[('A1312', 'PRO 1')].type)
print(dat3.generator[('A1312', 'PRO 1')].gx)
print(dat3.generator[('A1312', 'PRO 1')].ex)

dat3.clear_generators()
dat3.add_generator(gen_inj)
dat3.add_generator(gen_pro)

print(dat3.generator)
print(dat3.generator[('  a 8', 'INJ 1')].block)
print(dat3.generator[('  a 8', 'INJ 1')].type)
print(dat3.generator[('  a 8', 'INJ 1')].gx)
print(dat3.generator[('  a 8', 'INJ 1')].ex)
print(dat3.generator[('  l 3', 'PRO 1')].block)
print(dat3.generator[('  l 3', 'PRO 1')].type)
print(dat3.generator[('  l 3', 'PRO 1')].gx)
print(dat3.generator[('  l 3', 'PRO 1')].ex)

# 印刷間隔変更
dat3.parameter['print_interval'] = 1

dat3.parameter['max_timesteps'] = 100
print(dat3.parameter)


dat3.write('rvf3.dat')


#----sedでファイル編集
# SAVEの中の+++以降を削除 空白に置換
os.system('sed "/^+++/q" rvf2_SAVE | sed s/+++// > rvf2_restart')
# tough2 data file の中のINCON行を削除
os.system('sed -i -e "/INCON/d" rvf3.dat ')

#### add 20190707 element CONDBの追加
# 無理やり改行追加 (for semi-analytical heat ex. calc.)
os.system("sed -e '142 s/^/@/' rvf3.dat | tr '@' '\n' > tmp" )
os.system("sed -e '142 s/^/con00          CONBD/' tmp | tr '@' '\n' > rvf3.dat" )


"""
# MOP(13) = 2に変更←必要なし 
# MOP(13) = 0 *** DEFINES CONTENT OF INCON AND SAVE FILE.
#           = 0: STANDARD CONTENT.
#           = 2: READS PARAMETERS OF HYSTERESIS MODEL FROM FILE INCON.

print("MOP13")
print(dat3.parameter['option'])
print(dat3.parameter['option'][12])
dat3.parameter['option'][12] = 2
"""

# --- run the model ------------------------------------
import os
file = os.path.abspath('rvf3.dat')
inconfile = os.path.abspath('rvf2_restart')
os.system('t2exec_with_incon 1 ' + file + ' ' + inconfile)


lst3 = t2listing('rvf3.listing')
lst3.write_vtk(geo, 'rvf3.vtu', None)