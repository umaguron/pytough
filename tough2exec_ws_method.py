from t2data import *
from t2incons import *
from t2listing import *
import os
import datetime
import math
from Region import *

class tough2exec_ws_method(object): 
    # def __init__(self):
    #     print("init")
        
    def test(self):
        print("test")

    def run(self, \
        dat_file_base_name, \
        usesSaveAsIncon, \
        inconDir, \
        makesInconFile, \
        dat_title, \
        max_timesteps, \
        print_level, \
        tstop, \
        const_timestep, \
        print_interval, \
        gravity, \
        P_air, \
        T_air, \
        PCO2_air, \
        P_defaut, \
        T_defaut, \
        PCO2_default, \
        q, \
        around_perm, \
        minc_fracture_perm, \
        minc_matrix_perm, \
        makes_minc_block, \
        volume_fraction, \
        fracture_spacing, \
        number_fracture_planes, \
        injects_unrest, \
        CO2_unrest_flux, \
        CO2_unrest_enthalpy, \
        WATE_unrest_flux, \
        WATE_unrest_enthalpy, \
        injects_static, \
        CO2_static_flux, \
        CO2_static_enthalpy, \
        WATE_static_flux, \
        WATE_static_enthalpy, \
    ):

        geofilename = 'geo.dat'

        # どちらもTrueの場合はエラーにする
        if usesSaveAsIncon and makesInconFile:
            sys.exit("INCON error")
        # 既存のSAVEをINCONとして使用する場合、存在チェックをする
        if usesSaveAsIncon and not os.path.exists(inconDir):
            sys.exit("no such INCON directory")

        # 日付取得
        begin = datetime.datetime.today()
        now = str(begin.strftime("%Y%m%d-%H%M%S"))

        # 保存ディレクトリを作成
        t2FileDir = 't2out_' + now + "_" + dat_file_base_name + '/'
        os.makedirs(t2FileDir)
        os.chdir(t2FileDir)
        # はじめに自分自身をコピー
        import shutil
        shutil.copy2(__file__, os.path.splitext(os.path.basename(__file__))[0] + "_" + now + ".py")

        #各種ファイル名
        dat_file_base_name = dat_file_base_name + '_' + now
        geoFileRelPath = geofilename
        gridVtuRelPath =  dat_file_base_name + '_grid.vtu'
        datFileRelPath = dat_file_base_name + '.dat'
        listingFileRelPath = now + '.listing'
        restartInconRelPath = 'INCON'
        outputVtuRelPath = dat_file_base_name + '.vtu'
        makeOutVtuRelPath = 'makekVtu.py'
        execCommUpTxtRelPath = 'commandsUp.txt'
        execCommExecTxtRelPath = 'commandsExec.txt'
        execCommDownTxtRelPath = 'commandsDown.txt'
        afterCalcRelPath = 'afterCalc.py'
        logFileRelPath = 'log.tsv'


        # メッシュ作成
        # ↓*******************************************************************↓
        # make mulgird
        dx = 200.
        nx = 25
        dy = 200.
        ny = 25



        #        -+-------------+---------+
        #        ↑|             |         |
        #   xarr1 |             |         |  
        #        ↓|             |         |
        #        -+-------------|---------+ 
        #        ↑|             |         |
        # xarrCore|  pos. inj-> *         |  
        #        ↓|             |         |
        #        -+-------------|---------+ 
        #        ↑|             |         |
        #   xarr2 |             |         | 
        #        ↓|             |         |
        #        -+-------------+---------+ 
        #         |<-  yarr1  ->|<-yarr2->| 

        """ xarr1 = np.logspace(math.log10(200), math.log10(1000), 5)
        xarrCore = [200]*9
        xarr2 = np.flip(xarr1)
        xarr = np.r_[xarr2, xarrCore, xarr1] """
        xarr0 = np.logspace(math.log10(600), math.log10(4000), 5)
        xarr1 = np.logspace(math.log10(400), math.log10(1000), 4)
        xarrCore = [400]*5
        xarr2 = np.flip(xarr1)
        xarr3 = np.flip(xarr0)
        xarr = np.r_[xarr2, xarrCore, xarr1]
        yarr1 = np.logspace(math.log10(1500), 1, 20)
        yarr2 = np.logspace(math.log10(19), math.log10(1000), 7)
        yarr = np.r_[yarr1, yarr2]

        """ dz1 = np.logspace(1., 2.3, 10)
        dz2 = [200]*20
        zarr = np.r_[dz1, dz2]
        """
        dz1 = np.logspace(math.log10(20), math.log10(400), 5)
        dz2 = [400]*10
        zarr = np.r_[dz1, dz2]

        geo_y_logscale = mulgrid().rectangular(xarr, yarr, zarr, atmos_type = 0)
        geo_y_logscale_200 = mulgrid().rectangular(\
            xarr, \
            np.r_[\
                np.logspace(math.log10(700), math.log10(200), 6), \
                [200]*13, \
                np.logspace(math.log10(200), 1, 6), \
                np.logspace(math.log10(19), math.log10(700), 7)\
            ], \
            zarr, \
            atmos_type = 0)

        geo_y_logscale_400 = mulgrid().rectangular(\
            xarr, \
            np.r_[\
                np.logspace(math.log10(1000), math.log10(400), 4), \
                [400]*6, \
                np.logspace(math.log10(400), math.log10(20), 4), \
                np.logspace(math.log10(54), math.log10(1000), 4)\
            ], \
            zarr, \
            atmos_type = 0)

        geo_y_logscale_400_2_dykes = mulgrid().rectangular(\
            np.r_[xarr3, xarr, xarr0], \
            np.r_[\
                np.logspace(math.log10(4000), math.log10(400), 6), \
                [400]*2, \
                np.logspace(math.log10(230), math.log10(20), 4), \
                np.logspace(math.log10(45), math.log10(230), 3), \
                [341]*3, \
                np.logspace(math.log10(230), math.log10(20), 4), \
                np.logspace(math.log10(45), math.log10(230), 3), \
                np.logspace(math.log10(400), math.log10(4000), 6)\
            ], \
            zarr, \
            atmos_type = 0)

        geo = geo_y_logscale_400_2_dykes
        # pos. injection
        # 使い所なしinj_pos = (np.sum(xarr1) + 1000, np.sum(yarr1), -3000)

        print("number ofxcxnnections")
        print(xarr.shape[0] * zarr.shape[0] * (yarr.shape[0] - 1)\
            + yarr.shape[0] * zarr.shape[0] * (xarr.shape[0] - 1)\
            + xarr.shape[0] * zarr.shape[0] * yarr.shape[0])

        # TOPは時間不変に設定するならnegativeにする
        geo.atmosphere_volume = 1e50
        geo.atmosphere_connection = 1e-9
        # ↑*******************************************************************↑
        geo.write(geoFileRelPath)


        # Create TOUGH2 input data file:
        dat = t2data()
        dat.filename = datFileRelPath
        dat.title = dat_title
        dat.grid = t2grid().fromgeo(geo)

        # fix TOP boundary condition
        dat.grid.demote_block('ATM 0')


        # ↓*******************************************************************↓
        # PARAM
        dat.parameter.update(
            {'max_timesteps': max_timesteps,
            'print_level': print_level,
            'tstop': tstop,
            'const_timestep': const_timestep,
            'print_interval': print_interval,
            'gravity': gravity,
            'default_incons': [P_defaut, T_defaut, PCO2_default]})

        # Set MOPs:
        dat.parameter['option'][1] = 1 # *** ALLOWS TO GENERATE A SHORT PRINTOUT FOR EACH NEWTON-RAPHSON ITERATION

        #   MORE PRINTOUT IS GENERATED FOR MOP(I) > 0 IN THE FOLLOWING SUBROUTINES (THE LARGER MOP IS, THE MORE WILL BE PRINTED)
        dat.parameter['option'][2] = 0 # *** CYCIT
        dat.parameter['option'][3] = 0 # *** MULTI 
        dat.parameter['option'][4] = 0 # *** QU
        dat.parameter['option'][5] = 0 # *** EOS
        dat.parameter['option'][6] = 0 # *** LINEQ
        dat.parameter['option'][8] = 0 # *** DISF (T2DM ONLY)

        dat.parameter['option'][7] = 0 # *** IF UNEQUAL ZERO, WILL GENERATE A PRINTOUT OF INPUT DATA
        dat.parameter['option'][9] = 0 # *** CHOOSES FLUID COMPOSITION ON WITHDRAWAL (PRODUCTION). = 0: ACCORDING TO RELATIVE MOBILITIES.= 1: ACCORDING TO COMPOSITION IN PRODUCING ELEMENT.
        dat.parameter['option'][11] = 0 # *** CHOOSES EVALUATION OF MOBILITY AND ABSOLUTE PERMEABILITY AT INTERFACES. = 0: MOBILITIES ARE UPSTREAM WEIGHTED WITH WUP. (DEFAULT IS WUP = 1.0). PERMEABILITY IS UPSTREAM WEIGHTED., = 1: MOBILITIES ARE AVERAGED BETWEEN ADJACENT ELEMENTS. PERMEABILITY IS UPSTREAM WEIGHTED., = 2: MOBILITIES ARE UPSTREAM WEIGHTED WITH WUP. (DEFAULT IS WUP = 1.0). PERMEABILITY IS HARMONIC WEIGHTED., = 3: MOBILITIES ARE AVERAGED BETWEEN ADJACENT ELEMENTS. PERMEABILITY IS HARMONIC WEIGHTED., = 4: MOBILITY * PERMEABILITY PRODUCT IS HARMONIC WEIGHTED.
        dat.parameter['option'][16] = 5 # *** PERMITS TO CHOOSE TIME STEP SELECTION OPTION  = 0: USE TIME STEPS EXPLICITLY PROVIDED AS INPUT. > 0: INCREASE TIME STEP BY AT LEAST A FACTOR 2, IF CONVERGENCE OCCURS IN .LE. MOP(16) ITERATIONS.
        dat.parameter['option'][17] = 9 #  *** HANDLES TIME STEPPING AFTER LINEAR EQUATION SOLVER FAILURE. = 0: NO TIME STEP REDUCTION DESPITE LINEAR EQUATION SOLUTION FAILURE. = 9: REDUCE TIME STEP AFTER LINEAR EQUATION SOLUTION FAILURE.

        # set short printout
        # PARAM - ELST 
        dat.parameter['print_block'] = ' sa15'
        dat.parameter['print_block'] = ' sa11'
        dat.parameter['print_block'] = ' sa 5'
        dat.parameter['print_block'] = ' je 6'
        # FOFT
        dat.history_block = [' sa15',' sa11',' sa 5',' je 6']

        dat.start = True

        # MULTI 
        dat.multi = {
            'num_components': 2,
            'num_equations': 3,
            'num_phases': 2,
            'num_secondary_parameters': 6} 


        # ROCKS
        # define rock
        # reservoir = rocktype(name = "resvr", nad = 0, density = 2650.0, porosity = 0.1, permeability = [1e-12]*3, conductivity = 2.1, specific_heat = 1000.0)
        around = rocktype(name = "arnd", nad = 0, density = 2650.0, porosity = 0.05, permeability = around_perm, conductivity = 2.1, specific_heat = 1000.0)
        minc_fracture = rocktype(name = "fminc", nad = 0, density = 2650.0, porosity = 0.1, permeability = minc_fracture_perm, conductivity = 2.1, specific_heat = 1000.0)
        minc_matrix = rocktype(name = "Xminc", nad = 0, density = 2650.0, porosity = 0.05, permeability = minc_matrix_perm, conductivity = 2.1, specific_heat = 1000.0)
        atmos = rocktype(name = "atmos", nad = 2, density = 2650.0, porosity = 0.9999, permeability = [0, 0, 1e-12], conductivity = 2.51, specific_heat = 100000.0)
        atmos.relative_permeability = {'parameters': [0.1, 0.0, 1.0, 0.1], 'type': 1}
        atmos.capillarity = {'parameters':[0.0, 0.0, 1.0], 'type':1}
        atmos.tortuosity = 1.0
        dat.grid.add_rocktype(minc_fracture)
        dat.grid.add_rocktype(around)
        dat.grid.add_rocktype(atmos)
        dat.grid.add_rocktype(minc_matrix)


        # GENERの設定
        #gen_inj_unrest_co2 = t2generator(name = 'INJ 2', block = ' qx22', type = 'COM2', gx = 5.2, ex = 3.0e+5 ) #グリッドlogscale_200, 貯留槽の底がqx22 温度300度
        """ gen_inj_unrest_co2 = t2generator(name = 'INJ 2', block = ' qx22', type = 'COM2', gx = 500, ex = 3.0e+5 ) #グリッドlogscale_200, 貯留槽の底がqx22 温度300度
        gen_inj_unrest_h2o = t2generator(name = 'INJ 1', block = ' qx22', type = 'WATE', gx = 10000., ex = 1.26e+6 ) #温度300度
        """
        """ # geo 400
        gen_inj_unrest_co2 = t2generator(name = 'INJ 2', block = ' ft11', type = 'COM2', gx = CO2_unrest_flux, ex = CO2_unrest_enthalpy ) #グリッドlogscale_400, 貯留槽の底がft11 温度300度
        gen_inj_unrest_h2o = t2generator(name = 'INJ 1', block = ' ft11', type = 'WATE', gx = WATE_unrest_flux, ex = WATE_unrest_enthalpy ) #温度300度
        """
        # geo 400m 2dykes
        gen_static_co2 = t2generator(name = 'INJ 2', block = ' sa15', type = 'COM2', gx = CO2_static_flux, ex = CO2_static_enthalpy ) #温度300度
        gen_static_h2o = t2generator(name = 'INJ 1', block = ' sa15', type = 'WATE', gx = WATE_static_flux, ex = WATE_static_enthalpy ) #温度300度
        gen_inj_unrest_co2 = t2generator(name = 'INJ 2', block = ' sa11', type = 'COM2', gx = CO2_unrest_flux, ex = CO2_unrest_enthalpy ) #貯留槽の底がft11 温度300度
        gen_inj_unrest_h2o = t2generator(name = 'INJ 1', block = ' sa11', type = 'WATE', gx = WATE_unrest_flux, ex = WATE_unrest_enthalpy ) #温度300度


        if injects_unrest:
            dat.add_generator(gen_inj_unrest_co2)
            dat.add_generator(gen_inj_unrest_h2o)

        if injects_static:
            dat.add_generator(gen_static_co2)
            dat.add_generator(gen_static_h2o)


        # crustal heat flow
        layer = geo.layerlist[-1]  # bottom layer
        for col in geo.columnlist:
            blockname = geo.block_name(layer.name, col.name)
            print(blockname + '_' + str(q*col.area))
            crust_hf = t2generator(name = blockname, block = blockname, type = 'HEAT', gx = q*col.area)
            dat.add_generator(crust_hf)
        #↑*******************************************************************↑


        # INCON
        dat.incon = None
        if usesSaveAsIncon and os.path.exists(inconDir):
            # SAVEファイルをINCONとして使用する場合は今回のフォルダにINCONの形式で書き出す。
            t2incon(inconDir + 'SAVE').write(restartInconRelPath)
            print("\nUSE SAVE AS INCON\n")
        elif makesInconFile :
            # 任意のINCONを作成する場合
            """ # 全グリッドに同じ値を割り当て
            inc = dat.grid.incons(values =(P_defaut, T_defaut, PCO2_default)) """
            # ↓*******************************************************************↓
            # Set up and write initial conditions file with approximate
            # hydrostatic pressure profile:
            inc = dat.grid.incons()
            rho = 998.
            g = 9.8
            P0 = P_defaut
            T0 = T_defaut
            PCO20 = PCO2_default
            for blk in dat.grid.blocklist:
                # get depth if block centre is defined-
                # otherwise use zero (atmosphere block)
                h = -blk.centre[2] if blk.centre is not None else 0.
                P = P0 + rho * g * h
                T = T0
                PCO2 = PCO20
                inc[blk.name].variable = [P, T, PCO2]
            inc['ATM 0'].variable = [P_air, T_air, PCO2_air]
            # ↑*******************************************************************↑
            inc.write('INCON')
            print("\nUSE MANUAL INCON\n")
        else:
            dat.incon = t2incon()
            print("\nUSE DEFAULT INCON\n")

        # rocktype割当て
        # ↓*******************************************************************↓
        # 割り当てる領域の範囲（中心座標）を指定
        #resv = Region(2000, 4000, 2000, 5000, -3000, -1500)
        """
        resv = Region( \
            xmin = np.sum(xarr1), \
            xmax = np.sum(xarr1) + 2000, \
            ymin = np.sum(yarr1) - 3000, \
            ymax = np.sum(yarr1), \
            zmin = -3000, \
            zmax = -1500 \
            )
        """
        """ 
        # geo_y_logscale_400
        resv = Region( \
            xmin = np.sum(xarr1), \
            xmax = np.sum(xarr1) + 1800, \
            ymin = np.sum(np.logspace(math.log10(1000), math.log10(400), 4)) + 2400 + np.sum(np.logspace(math.log10(400), math.log10(20), 4)) - 3000, \
            ymax = np.sum(np.logspace(math.log10(1000), math.log10(400), 4)) + 2400 + np.sum(np.logspace(math.log10(400), math.log10(20), 4)), \
            zmin = -3000, \
            zmax = -1500 \
            )
        """
        # geo_y_logscale_400_2_dykes
        resv = Region( \
            xmin = np.sum(np.r_[xarr1, xarr0]), \
            xmax = np.sum(np.r_[xarr1, xarr0]) + 2000, \
            ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(101.734), 2)) - 3000, \
            ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(101.734), 2)), \
            zmin = -3000, \
            zmax = -1500 \
            )
        dyke_ms = Region( \
            xmin = np.sum(np.r_[xarr1, xarr0]) + 800, \
            xmax = np.sum(np.r_[xarr1, xarr0]) + 1200, \
            ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) - 20, \
            ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)), \
            zmin = -1500, \
            zmax = -740 \
            )
        dyke_sn = Region( \
            xmin = np.sum(np.r_[xarr1, xarr0]) + 800, \
            xmax = np.sum(np.r_[xarr1, xarr0]) + 1200, \
            ymin = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) - 20, \
            ymax = np.sum(np.logspace(math.log10(4000), math.log10(400), 6)) + 800 + np.sum(np.logspace(math.log10(230), math.log10(20), 4)) + np.sum(np.logspace(math.log10(45), math.log10(230), 3)) + 341 * 3 + + np.sum(np.logspace(math.log10(230), math.log10(20), 4)),\
            zmin = -10000, \
            zmax = -341 \
            )
        # ↑*******************************************************************↑
        # マトリックス
        for blk in dat.grid.blocklist:
            blk.rocktype = around

        # apply rock type
        for blk in dat.grid.blocklist:
            if 'ATM' not in blk.name :
                # resevoir
                if resv.xmin <= blk.centre[0] <= resv.xmax \
                        and resv.ymin <= blk.centre[1] <= resv.ymax \
                        and resv.zmin <= blk.centre[2] <= resv.zmax :
                    print("reservoir")
                    print(blk.centre)
                    blk.rocktype = minc_fracture
                    continue
                # dyke
                if dyke_ms.xmin <= blk.centre[0] <= dyke_ms.xmax \
                        and dyke_ms.ymin <= blk.centre[1] <= dyke_ms.ymax \
                        and dyke_ms.zmin <= blk.centre[2] <= dyke_ms.zmax :
                    print("dyke_ms")
                    print(blk.centre)
                    blk.rocktype = minc_fracture
                    continue
                if dyke_sn.xmin <= blk.centre[0] <= dyke_sn.xmax \
                        and dyke_sn.ymin <= blk.centre[1] <= dyke_sn.ymax \
                        and dyke_sn.zmin <= blk.centre[2] <= dyke_sn.zmax :
                    print("dyke_sn")
                    print(blk.centre)
                    blk.rocktype = minc_fracture


        # atmosphere
        for blk in dat.grid.blocklist:
            if 'ATM' in blk.name :
                blk.rocktype = atmos


        # MINC
        if makes_minc_block:
            minc_block_list = []
            for blk in dat.grid.blocklist:
                if 'ATM' not in blk.name :
                    # reservoir
                    if resv.xmin <= blk.centre[0] <= resv.xmax \
                            and resv.ymin <= blk.centre[1] <= resv.ymax \
                            and resv.zmin <= blk.centre[2] <= resv.zmax :
                        minc_block_list.append(blk.name)
                        continue
                    # dyke
                    if dyke_ms.xmin <= blk.centre[0] <= dyke_ms.xmax \
                            and dyke_ms.ymin <= blk.centre[1] <= dyke_ms.ymax \
                            and dyke_ms.zmin <= blk.centre[2] <= dyke_ms.zmax :
                        minc_block_list.append(blk.name)
                        continue
                    if dyke_sn.xmin <= blk.centre[0] <= dyke_sn.xmax \
                            and dyke_sn.ymin <= blk.centre[1] <= dyke_sn.ymax \
                            and dyke_sn.zmin <= blk.centre[2] <= dyke_sn.zmax :
                        minc_block_list.append(blk.name)

            # 既存のグリッドをMINC対応版に更新する
            minc_indices = dat.grid.minc(\
                # [vol fraction of FRACTURE, vol fraction of MINC level 1, ...]
                volume_fraction, \
                # fractureの間隔(m) num_fracture_planes>=2のときはlist
                spacing=fracture_spacing, \
                # 1, 2 or 3
                num_fracture_planes=number_fracture_planes, \
                # block name list
                blocks=minc_block_list, \
                # MINC　matrixのblocknameを返す関数。Noneの場合もともとの名前のはじめ１文字をMINC levelの数字に書き換え
                matrix_blockname=None, \
                # MINC　matrixのrock nameを返す関数. Noneの場合もともとのROCK名のはじめ１文字を'X'に書き換え
                minc_rockname=None, \
                # fracture面から与えられた長さ以内にあるmatrixの総体積。 Noneの場合num_fracture_planesに応じて自動で計算
                proximity=None, \
                # 空気層の体積
                atmos_volume=1e50, \
                # t2inconを与えると適切に書き換えてくれる
                incon=dat.incon\
            )
        """ 
        #実験（MINC)
        print(minc_indices)
        print(dat.grid.blockmap(geo, index=minc_indices[1]))
        """
        #グリッド確認用
        """ geo.layer_plot(-4000., block_names=True, rocktypes=dat.grid)
        geo.layer_plot(-3199., block_names=True, rocktypes=dat.grid)
        geo.layer_plot(-2799., block_names=True, rocktypes=dat.grid)
        geo.layer_plot(-1200., block_names=True, rocktypes=dat.grid)
        geo.layer_plot(-700., block_names=True, rocktypes=dat.grid) """

        # 書き出し
        # !!!注意!!!paraviewにてrocktypeの表示が何故か１個ずれる
        dat.grid.write_vtk(geo, gridVtuRelPath)
        dat.write(datFileRelPath)
        # sys.exit()
        # sys.exit("test")


        # --- run the model ------------------------------------
        datFileAbsPath = os.path.abspath(datFileRelPath)
        curAbsPath = os.path.abspath(os.path.curdir) + '/'
        print("RUN")
        # 使うまでもなかった↓
        # dat.run(simulator='../xt2_eos2', output_filename=now + '.listing')
        os.system('../xt2_eos2' + ' < ' + dat.filename + ' | tee ' + listingFileRelPath)


        # listingを読んでVTUを作成するコードを作成
        if os.path.exists(listingFileRelPath):
            lst2 = t2listing(listingFileRelPath)
            tu = "d"
            if dat.parameter['print_level'] <= 1 :
                lst2.write_vtk(geo, curAbsPath + outputVtuRelPath, None, time_unit=tu)
            else :
                lst2.write_vtk(geo, curAbsPath + outputVtuRelPath, dat.grid, None, False, time_unit=tu, blockmap=dat.grid.rectgeo()[1])
        else:
            print("no listing file")

        # ログに書き込み
        log = open('../' + logFileRelPath, 'a')
        end = datetime.datetime.today()
        duration =  end - begin
        log.write(t2FileDir + "\t" \
            + str(inconDir if usesSaveAsIncon else 'hydrostatic' ) + "\t" \
            + str(dat.grid.num_blocks) + "\t" \
            + str() +  "\t" \
            + str() +  "\t" \
            + str() +  "\t" \
            + str() +  "\t" \
            + str() +  "\t" \
            + str(around_perm) +  "\t" \
            + str(minc_fracture_perm) +  "\t" \
            + str(minc_matrix_perm) +  "\t" \
            + str(volume_fraction if makes_minc_block else '-') +  "\t" \
            + str(fracture_spacing if makes_minc_block else '-') +  "\t" \
            + str(number_fracture_planes if makes_minc_block else '-') +  "\t" \
            + str(CO2_unrest_flux if injects_unrest else '-') +  "\t" \
            + str(CO2_unrest_enthalpy if injects_unrest else '-') +  "\t" \
            + str(WATE_unrest_flux if injects_unrest else '-') +  "\t" \
            + str(WATE_unrest_enthalpy if injects_unrest else '-') +  "\t" \
            + str(CO2_static_flux if injects_static else '-') +  "\t" \
            + str(CO2_static_enthalpy if injects_static else '-') +  "\t" \
            + str(WATE_static_flux if injects_static else '-') +  "\t" \
            + str(WATE_static_enthalpy if injects_static else '-') +  "\t" \
            + str(q) +  "\t" \
            + str((lst2.index + 1) * print_interval) +  "\t" \
            + str(lst2.time)  + tu + "\t" \
            + str(duration.total_seconds()) + "s\n" )
        log.close()

        # --- plot -----------


        #※引数の'variable'については、バグなのかわからないが要素が一個ずれるのでnp.insertを使ってndarrayの先頭にダミー要素を追加している
        # geo.layer_plot(-600, np.insert(lst.element['T'], 0, 0), 'Temperature', '$\degree$C', contours = np.arange(100,200,25), flow=lst.connection['FLO(AQ.)'])
        # geo.slice_plot([np.array([0, 550]), np.array([1100, 550])], np.insert(lst.element['T'], 0, 0), 'Temperature', '$\degree$C', flow=lst.connection['FLO(AQ.)'], )
        #geo.line_plot([500, 500, 0], [500, 500, 881.1], lst3.)




