from tough2exec_ws_method import *




tough2exec_ws_method().run(dat_file_base_name = '100_0p05_0p1_-11', \
    usesSaveAsIncon = False, \
    inconDir = "/Users/matsunagakousei/Documents/training/python/pyTough/t2out_20190904-215436_no_minc_mesh_logscale_400_2_dykes_hydrostatic/", \
    makesInconFile = True, \
    dat_title = 'inj test 3d 1', \
    max_timesteps = 9990, \
    print_level = 0, \
    tstop = 2.e20, \
    const_timestep = 1.e2, \
    print_interval = 1, \
    gravity = 9.81, \
    P_air = 1e5, \
    T_air = 10, \
    PCO2_air = 1e5 * 0.0004, \
    P_defaut = 1.013e5, \
    T_defaut = 10, \
    PCO2_default = 1.013e5 * 0.0004, \
    q = 100e-3 , \
    around_perm = [1e-15]*3, \
    minc_fracture_perm = [1e-12]*3, \
    minc_matrix_perm = [1e-15]*3, \
    makes_minc_block = True, \
    volume_fraction = [0.01, 0.99], \
    fracture_spacing = 1, \
    number_fracture_planes = 1, \
    injects_unrest = False, \
    CO2_unrest_flux = 0.052, \
    CO2_unrest_enthalpy = 3.0e+5, \
    WATE_unrest_flux = 50, \
    WATE_unrest_enthalpy = 1.26e+6, \
    injects_static = False, \
    CO2_static_flux = 0.052, \
    CO2_static_enthalpy = 3.0e+5, \
    WATE_static_flux = 3.0e+5, \
    WATE_static_enthalpy = 1.26e+6, \
)

begin = datetime.datetime.today()
now = str(begin.strftime("%Y%m%d-%H%M%S"))

# 自分自身をコピー
import shutil
shutil.copy2(__file__, os.path.splitext(os.path.basename(__file__))[0] + "_" + now + ".py")